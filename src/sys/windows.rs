use core::ffi::{c_int, c_void, c_uint};

extern "C" {
    fn _read(fd: c_int, buffer: *mut c_void, buffer_size: c_uint) -> c_int;
    fn _write(fd: c_int, buffer: *const c_void, count: c_uint) -> c_int;

    #[link_name = "_isatty"]
    pub fn isatty(fd: c_int) -> c_int;
}

pub unsafe fn read(fd: c_int, buf: *mut c_void, count: usize) -> isize {
    let result = unsafe { _read(fd, buf, count.try_into().unwrap_or(u32::MAX)) };
    result as isize
}

pub unsafe fn write(fd: c_int, buf: *const c_void, count: usize) -> isize {
    let result = unsafe { _write(fd, buf, count.try_into().unwrap_or(u32::MAX)) };
    result as isize
}

pub const STDIN_FILENO: c_int = 0;
pub const STDOUT_FILENO: c_int = 1;
pub const STDERR_FILENO: c_int = 2;
