#![no_std]

use core::mem::MaybeUninit;

pub use generic_io::{Read, ReadCursor, Write, WriteCursor};

mod sys;

pub struct Handle<const READ: bool, const WRITE: bool> {
    fd: core::ffi::c_int,
}

impl<const READ: bool, const WRITE: bool> Handle<READ, WRITE> {
    pub fn is_tty(&self) -> bool {
        // SAFETY: we have valid fd
        let result = unsafe { sys::isatty(self.fd) };
        result != 0
    }
}

pub type ReadHandle = Handle<true, false>;
pub type WriteHandle = Handle<false, true>;
pub type ReadWriteHandle = Handle<true, true>;

impl ReadHandle {
    /// # Safety
    /// Ensure that at most one owner exists, and it wasn't closed
    pub const unsafe fn stdin() -> Self {
        Self { fd: sys::STDIN_FILENO }
    }
}

impl WriteHandle {
    /// # Safety
    /// Ensure that at most one owner exists, and it wasn't closed
    pub const unsafe fn stdout() -> Self {
        Self { fd: sys::STDOUT_FILENO }
    }

    /// # Safety
    /// Ensure that at most one owner exists, and it wasn't closed
    pub const unsafe fn stderr() -> Self {
        Self { fd: sys::STDERR_FILENO }
    }
}

#[derive(Debug)]
pub struct ReadError(());

impl Read for ReadHandle {
    type Error = ReadError;

    fn read(&mut self, cursor: &mut ReadCursor) -> Result<(), Self::Error> {
        let cursor_written = self.raw_read(cursor.uninit_mut())?;
        // SAFETY: length taken from uninit part
        unsafe { cursor.advance(cursor_written) };
        Ok(())
    }
}

impl ReadHandle {
    fn raw_read(&mut self, buf: &mut [MaybeUninit<u8>]) -> Result<usize, ReadError> {
        let ptr = buf.as_mut_ptr();
        // SAFETY: correct fd, pointer and length taken from slice
        let result = unsafe { sys::read(self.fd, ptr.cast(), buf.len()) };
        if result.is_negative() {
            Err(ReadError(()))
        } else {
            Ok(result as usize)
        }
    }
}

#[derive(Debug)]
pub struct WriteError(());

impl WriteHandle {
    fn raw_write(&mut self, buf: &[u8]) -> Result<usize, WriteError> {
        let ptr = buf.as_ptr();
        // SAFETY: every argument is valid
        let result = unsafe { sys::write(self.fd, ptr.cast(), buf.len()) };
        if result.is_negative() {
            Err(WriteError(()))
        } else {
            Ok(result as usize)
        }
    }
}

impl Write for WriteHandle {
    type Error = WriteError;

    fn write(&mut self, cursor: &mut WriteCursor) -> Result<(), Self::Error> {
        let cursor_written = self.raw_write(cursor.not_written())?;
        // SAFETY: length taken from not written part
        unsafe { cursor.advance(cursor_written) };
        Ok(())
    }
}
